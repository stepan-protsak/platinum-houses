<?php get_header(); ?>
<?php

global $nectar_theme_skin, $options;

$bg = get_post_meta($post->ID, '_nectar_header_bg', true);
$bg_color = get_post_meta($post->ID, '_nectar_header_bg_color', true);
$fullscreen_header = (!empty($options['blog_header_type']) && $options['blog_header_type'] == 'fullscreen' && is_singular('post')) ? true : false;
$blog_header_type = (!empty($options['blog_header_type'])) ? $options['blog_header_type'] : 'default';
$fullscreen_class = ($fullscreen_header == true) ? "fullscreen-header full-width-content" : null;
$theme_skin = (!empty($options['theme-skin']) && $options['theme-skin'] == 'ascend') ? 'ascend' : 'default';
$hide_sidebar = (!empty($options['blog_hide_sidebar'])) ? $options['blog_hide_sidebar'] : '0';
$blog_type = $options['blog_type'];

if(have_posts()) : while(have_posts()) : the_post();

    nectar_page_header($post->ID);

endwhile; endif;



if($fullscreen_header == true) {

    if(empty($bg) && empty($bg_color)) { ?>
        <div class="not-loaded default-blog-title fullscreen-header" id="page-header-bg" data-midnight="light" data-alignment="center" data-parallax="0" data-height="450" style="height: 450px;">
            <div class="container">
                <div class="row">
                    <div class="col span_6 section-title blog-title">
                        <h1 class="entry-title"><?php the_title(); ?></h1>
                        <div class="author-section">
						 	<span class="meta-author">
						 		<?php if (function_exists('get_avatar')) { echo get_avatar( get_the_author_meta('email'), 100 ); }?>
						 	</span>
                            <div class="avatar-post-info vcard author">
                                <span class="fn"><?php the_author_posts_link(); ?></span>
                                <span class="meta-date date updated"><i><?php echo get_the_date(); ?></i></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            $button_styling = (!empty($options['button-styling'])) ? $options['button-styling'] : 'default';
            if($button_styling == 'default'){
                echo '<div class="scroll-down-wrap"><a href="#" class="section-down-arrow"><i class="icon-salient-down-arrow icon-default-style"> </i></a></div>';
            } else {
                echo '<div class="scroll-down-wrap"><a href="#" class="section-down-arrow"><i class="fa fa-angle-down top"></i><i class="fa fa-angle-down"></i></a></div>';
            }
            ?>
        </div>
    <?php }


    if($theme_skin != 'ascend') { ?>
        <div class="container">
            <div id="single-below-header" class="<?php echo $fullscreen_class; ?> custom-skip">
                <span class="meta-share-count"><i class="icon-default-style steadysets-icon-share"></i> <?php echo '<a href=""><span class="share-count-total">0</span> <span class="plural">'. __('Shares',NECTAR_THEME_NAME) . '</span> <span class="singular">'. __('Share',NECTAR_THEME_NAME) .'</span></a>'; nectar_blog_social_sharing(); ?> </span>
                <span class="meta-category"><i class="icon-default-style steadysets-icon-book2"></i> <?php the_category(', '); ?></span>
                <span class="meta-comment-count"><i class="icon-default-style steadysets-icon-chat-3"></i> <a href="<?php comments_link(); ?>"><?php comments_number( __('No Comments', NECTAR_THEME_NAME), __('One Comment ', NECTAR_THEME_NAME), __('% Comments', NECTAR_THEME_NAME) ); ?></a></span>
            </div><!--/single-below-header-->
        </div>

    <?php }

} ?>

<div class="container-wrap <?php echo ($fullscreen_header == true) ? 'fullscreen-blog-header': null; ?> <?php if($blog_type == 'std-blog-fullwidth' || $hide_sidebar == '1') echo 'no-sidebar'; ?>">
    <div class="container main-content">
        <?php echo single_post(); ?>
    </div>
</div>
<div class="footer-contact-modal">
    <button class="close" type="button"> </button>
    <div class="modal-in">
        <h1>Contactez-nous</h1>
        <?php echo do_shortcode('[contact-form-7 id="292" title="CONTACTEZ-NOUS"]'); ?>
    </div>
</div>
<script>
    jQuery(document).ready(function () {
        jQuery('#top .menu-item a').each(function () {
            var oldHref = jQuery(this).attr('href');
            if (oldHref.indexOf('#') == 0) {
                jQuery(this).attr('href', '/' + oldHref);
            }
        });
    });
</script>
<style>
    #ajax-content-wrap {
        background: white;
    }
    input, textarea{
        background: transparent !important;
    }
    .footer-contact-modal .modal-in input[type='submit'] {
        display: block !important;
        margin: auto !important;
        width: 200px !important;
        padding: 15px 0 !important;
        font-size: 18px !important;
        text-transform: uppercase !important;
        font-weight: bold !important;
        background: #f27184 !important;
        border: none !important;
    }
    .owl-dot.active span{
        background: #f27184 !important;
    }
    span.wpcf7-not-valid-tip, .wpcf7-response-output.wpcf7-display-none.wpcf7-validation-errors {
        background: transparent !important;
    }
</style>
<?php get_footer(); ?>
