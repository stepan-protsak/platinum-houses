<?php function show_posts($attributes) { ?>
    <?php ob_start();?>

        <?php $posts = get_platinum_posts($attributes); ?>

        <div class="posts-row">
            <?php foreach ($posts as $post) { ?>
                <div class="post-wrap">
                    <div class="top-box">
                        <a href="<?php echo $post['href']; ?>"><img src="<?php echo $post['image'];?>" /></a>
                    </div>
                    <div class="bottom-box">
                        <div class="name">
                            <a href="<?php echo $post['href']; ?>" ><?php echo $post['name'];?></a>
                        </div>
                        <div class="price">
                            <a href="<?php echo $post['href']; ?>" ><?php echo $post['price'];?> €</a>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    <?php $output = ob_get_contents(); ?>
    <?php ob_end_clean(); ?>
    <?php return $output; ?>
<?php } ?>
