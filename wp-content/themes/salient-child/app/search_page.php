<?php function search_platinum_page() { ?>
    <?php $posts = search_platinum_posts(); ?>
    <?php ob_start();?>
        <!--<div id="search-box-mobile">
            <i class="fa fa-search" aria-hidden="true"></i>
        </div>-->
        <div class="search-posts-wrap">
            <?php if (!empty($posts['posts'])) { ?>
                <?php foreach ($posts['posts'] as $post) {?>
                    <div class="one-post">
                        <div class="left-block">
                            <a href="<?php echo $post['href']; ?>">
                                <?php if ($post['label']) { ?>
                                    <div class="exclusive" >
                                        <div class="exclusive-badge">
                                            <div><?php echo $post['label']; ?></div>
                                        </div>
                                        <div class="triangle">

                                        </div>
                                    </div>
                                <?php } ?>
                                <img src="<?php echo $post['image'];?>"/>
                            </a>
                        </div>
                        <div class="right-block">
                            <div class="block-title">
                                <div class="post-name">
                                    <a href="<?php echo $post['href']; ?>" ><?php echo $post['name']; ?></a>
                                </div>
                                <div class="post-ref">
                                    Réf. : <?php echo $post['ref_number'];?>
                                </div>
                                <div class="post-price">
                                    <?php echo $post['price'];?> €
                                </div>
                            </div>
                            <div class="description">
                                <?php echo $post['description']; ?>
                            </div>
                            <?php if (!empty($post['area']) || !empty($post['bedrooms']) || !empty($post['bathrooms']) || !empty($post['parking_type']) || !empty($post['access_handicap'])) { ?>
                                <div class="info">
                                    <?php if (!empty($post['area'])) { ?>
                                        <div class="left-info">
                                            Surface : <?php echo $post['area']; ?> m<sup>2</sup>
                                        </div>
                                    <?php } ?>
                                    <div class="right-info">
                                        <?php if (!empty($post['bedrooms'])) { ?>
                                            <div class="block-icon">
                                                <?php if ($post['property_type_value'] == 'office') {?>
                                                    <img src="/wp-content/themes/salient-child/images/icons/office.png"/>
                                                <?php } else {?>
                                                    <img src="/wp-content/themes/salient-child/images/icons/bed.png"/>
                                                <?php } ?>
                                                <div class="block-quantity"><?php echo $post['bedrooms']; ?></div>
                                            </div>
                                        <?php } ?>
                                        <?php if (!empty($post['bathrooms'])) { ?>
                                            <div class="block-icon">
                                                <img src="/wp-content/themes/salient-child/images/icons/shower.png"/>
                                                <div class="block-quantity"><?php echo $post['bathrooms']; ?></div>
                                            </div>
                                        <?php } ?>
                                        <?php if (!empty($post['parking_type'])) { ?>
                                            <div class="block-icon">
                                                <?php if ($post['parking_type'] == 'garage') { ?>
                                                    <img src="/wp-content/themes/salient-child/images/icons/garage.png"/>
                                                <?php } elseif ($post['parking_type'] == 'parking') {?>
                                                    <img src="/wp-content/themes/salient-child/images/icons/parking.png"/>
                                                <?php } elseif ($post['parking_type'] == 'none' && ($post['property_type_value'] != 'garage')) {?>
                                                    <img src="/wp-content/themes/salient-child/images/icons/garage_crossed.png"/>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                        <?php if (!empty($post['access_handicap'])) { ?>
                                            <div class="block-icon">
                                                <img src="/wp-content/themes/salient-child/images/icons/handicap.png"/>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php }?>
                            <div class="more">
                                <div class="property-type">
                                    <?php echo $post['property_type']; ?>
                                </div>
                                <a class="show-post" href="<?php echo $post['href']; ?>">
                                    Découvrir
                                </a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
        <?php if (!empty($posts['pagination'])) { ?>
            <div class="pagination">
                <?php echo $posts['pagination']; ?>
            </div>
        <?php } ?>
        <script>
            jQuery(document).ready(function () {
                jQuery('#top .menu-item a').each(function () {
                    var oldHref = jQuery(this).attr('href');
                    if (oldHref.indexOf('#') == 0) {
                        jQuery(this).attr('href', '/' + oldHref);
                    }
                });
            });
        </script>
    <?php $output = ob_get_contents(); ?>
    <?php ob_end_clean(); ?>
    <?php return $output; ?>
<?php } ?>
<?php function search_heading() { ?>
    <?php ob_start();?>
        <?php if (isset($_GET['property_for'])) { ?>
            <?php if ($_GET['property_for'] == 'for_sell') { ?>
                <span>EN VENTE</span>
            <?php } elseif($_GET['property_for'] == 'for_rent') {?>
                <span>EN LOCATION</span>
            <?php } ?>
        <?php } ?>
    <?php $output = ob_get_contents(); ?>
    <?php ob_end_clean(); ?>
    <?php return $output; ?>
<?php } ?>
