<?php
function location_auto_complete() {
    global $wpdb;
    $result['success'] = false;
    $result['locations'] = array();
    if (isset($_POST['auto_complete'])) {
        $name = $_POST['auto_complete'];
        $query = "SELECT meta_value as name
                  FROM {$wpdb->postmeta} WHERE (meta_key='locality'
                  OR meta_key='administrative_area_level_1')
                  AND LOWER(meta_value) LIKE LOWER('".$name."%')
                  GROUP BY meta_value";
        $locations = $wpdb->get_results($query,ARRAY_A);
        if (!empty($locations)) {
            foreach ($locations as $location) {
                $result['locations'][] = $location['name'];
            }
            $result['success'] = true;
        }
    }

//    echo '<pre>'.print_r($result,true).'</pre>';

    echo json_encode($result);
    exit;
}