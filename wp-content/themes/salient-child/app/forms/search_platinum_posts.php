<?php
function search_platinum_posts() {
    $posts_per_page = 6;
    /* variable with post-types to name */
    $property_type_object = get_field_object('field_58b004f87b3f0');
    $property_type_to_name = $property_type_object['choices'];

    $result = array();


    $post_type = array('for_rent','for_sell');

    /*$meta_query = array();
    if (empty($_GET['floor'])) {
        $meta_query[] = array(
            'key' => 'floor',
            'value' => 0,
            'type' => 'numeric',
            'compare' => 'NOT',
        );
    }*/
  
    if (!empty($_GET['access_handicap']) && is_numeric($_GET['access_handicap']) ) {
        $meta_query[] = array(
            'key' => 'access_handicap',
            'value' => $_GET['access_handicap'],
            'type' => 'numeric',
            'compare' => '=',
        );
    }
    if (!empty($_GET['elevator'])) {
        $meta_query[] = array(
            'key' => 'elevator',
            'value' => $_GET['elevator'],
            'compare' => '='
        );
    }
    if (!empty($_GET['area']) && is_numeric($_GET['area']) ) {
        $meta_query[] = array(
            'key' => 'area',
            'value' => $_GET['area'],
            'type' => 'numeric',
            'compare' => '>=',
        );
    }
    if (!empty($_GET['property_for'])) {
        $post_type = $_GET['property_for'];
    }
    if (!empty($_GET['bathrooms'])) {
        $meta_query[] = array(
            'key' => 'bathrooms',
            'value' => $_GET['bathrooms'],
            'type' => 'numeric',
            'compare' => '=',
        );
    }
    if (!empty($_GET['bedrooms'])) {
        if ($_GET['bedrooms'] == 'more') {
            $compare = '>=';
            $number_of_bedrooms = 5;
        }
        else {
            $compare = '=';
            $number_of_bedrooms = $_GET['bedrooms'];
        }

        $meta_query[] = array(
            'key' => 'rooms',
            'value' => $number_of_bedrooms,
            'type' => 'numeric',
            'compare' => $compare,
        );
    }

    if (!empty($_GET['property_type'])) {
        $meta_query[] = array(
            'key' => 'property_type',
            'value' => $_GET['property_type'],
            'compare' => '=',
        );
    }
    if (!empty($_GET['min_price']) or !empty($_GET['max_price'])) {

        if (empty($_GET['max_price'])) {
            $max_price = 99999999999999;
        }
        else {
            $max_price = $_GET['max_price'];
        }

        $meta_query[] = array(
            'key' => 'price',
            'value' => array($_GET['min_price'], $max_price),
            'type' => 'numeric',
            'compare' => 'BETWEEN'
        );

    }
    if (!empty($_GET['location'])) {
        $meta_query[] = array(
            'relation' => 'OR',
            array(
                'key'     => 'locality',
                'value'   => $_GET['location'],
                'compare' => 'LIKE'
            ),
            array(
                'key'     => 'administrative_area_level_1',
                'value'   => $_GET['location'],
                'compare' => 'LIKE'
            ),
            array(
                'key'     => 'route',
                'value'   => $_GET['location'],
                'compare' => 'LIKE'
            )
        );
    }

    $args['meta_query'] = $meta_query;

    if (empty($_GET['c_page'])) {
        $current_page = 1;
    }
    else {
        $current_page = $_GET['c_page'];
    }

    if ($current_page == 1) {
        $offset = 0;
    }
    else {
        $offset = ($current_page*($posts_per_page)) - ($posts_per_page);
    }

    $args = array(
        'post_type' => $post_type,
        'order' => 'DESC',
        'orderby' => 'date',
        'offset' => $offset,
        'posts_per_page' => $posts_per_page,
        'meta_query' => $meta_query
    );


    $posts = new WP_Query($args);

    $pagination_size = 9;

    if ($current_page > 1) {
        $from_page = $current_page - 1;
    } else {
        $from_page = 1;
    }

    $to_page = $from_page + $pagination_size;
    if ($posts->max_num_pages < $to_page) {
        $to_page = $posts->max_num_pages;
        $from_page = $to_page - $pagination_size;
        if ($from_page < 1) {
            $from_page = 1;
        }
    }

    $result['pagination'] = pagination_function($to_page, $current_page, $from_page);

    foreach ($posts->posts as $post) {
        $images = get_field('imagese',$post->ID);
        $price = get_field('price', $post->ID);

        if (!empty($images[0]['sizes']['portfolio-thumb'])) {
            $image = $images[0]['sizes']['portfolio-thumb'];
        }
        else {
            $image = '';
        }

        $property_type_value = get_field('property_type',$post->ID);
        $result['posts'][] = array(
            'id' => $post->ID,
            'name' => strtoupper($post->post_title),
            'image' => $image,
            'price' => number_format((float)$price, 0, ',', ' '),
//            'href' => get_site_url().'/property/?id='.$post->ID,
            'href' => get_permalink($post->ID),
            'description' => $post->post_content,
            'bedrooms' => get_field('bedrooms',$post->ID),
            'bathrooms' => get_field('bathrooms', $post->ID),
            'parking_type' => get_field('parking_type', $post->ID),
            'area' => get_field('area', $post->ID),
            'ref_number' => get_field('refference_number', $post->ID),
            'access_handicap' => get_field('access_handicap', $post->ID),
            'post_type' => $post->post_type,
            'property_type' => ucfirst(strtolower($property_type_to_name[$property_type_value])),
            'property_type_value' => $property_type_value,
            'label' => get_field('label', $post->ID)
        );
    }

    return $result;
}

function pagination_function($total, $current_page, $from_page = 1) {
    $result = '';

    if ($total > 1) {
        $array_of_pages = range($from_page,$total);
        $url = parse_url(curPageURL());
        parse_str($url['query'], $params);
        $site_url = $url['scheme'] . '://' . $url['host'] . $url['path'] . '?';

        $prev_active = ($current_page - 1) <= 0 ? 'not_active' : 'active';
        $next_active = ($current_page + 1) > $total ? 'not_active' : 'active';

        if ($prev_active == 'active') {
            $params['c_page'] -= 1;
            $prev_active_url = $site_url . http_build_query($params);
            $params['c_page'] += 1;
        } else {
            $prev_active_url = '';
        }

        if ($next_active == 'active') {
            $params['c_page'] += 1;
            $next_active_url = $site_url . http_build_query($params);
            $params['c_page'] -= -1;
        } else {
            $next_active_url = '';
        }

        $result .= '<a class="prev ' . $prev_active . '" href="' . $prev_active_url . '" > Précédent </a>';
        foreach ($array_of_pages as $page) {
            $params['c_page'] = $page;
            $active = $page == $current_page ? 'pag-active' : '';
            $result .= '<a class="' . $active . '" href="' . $site_url . http_build_query($params) . '">' . $page . '</a>';
        }
        $result .= '<a class="next ' . $next_active . '" href="' . $next_active_url . '" > Suivant </a>';
    }
    return $result;
}

function curPageURL() {
    $pageURL = 'http';
    if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}