<?php
function get_single_post($id = false) {
    $result = array();
    if (empty($id) && empty($_GET['id'])) {
        $id = get_the_ID();
    }
    if(isset($_GET['id']) or !empty($id)) {

        if (!empty($id)) {
            $post = get_post($id);
        } else {
            $post = get_post($_GET['id']);
        }

        if (empty($post)) {
            http_response_code(404);
            die();
        }

        /* property types */
        $property_type_field_object = get_field_object('property_type', $post->ID);
        $property_type_value = get_field('property_type', $post->ID);
        $property_type = ucfirst(strtolower($property_type_field_object['choices'][$property_type_value]));

        /* images */
        $images = get_field('imagese', $post->ID);
        $application_file = get_field('application_file', $post->ID);
        $thumbs = images_to_array($images, 'large_featured');

        /* votre contact */
        $contractor_field = get_field_object('field_58b03ae24eb65');
        $contractor_choices = $contractor_field['choices'];
        $contractor_value = get_field('contractor', $post->ID);
        if (isset($contractor_choices[$contractor_value])) {
            $contractor = $contractor_choices[$contractor_value];
        } else {
            $contractor = false;
        }

        /* price */
        $price = get_field('price', $post->ID);

        $result = array(
            'id' => $post->ID,
            'name' => strtoupper($post->post_title),
            'price' => number_format((float)$price, 0, ',', ' '),
            'price_for_search' => $price,
            'post_type' => $post->post_type,
            'images' => $thumbs,
            'description' => $post->post_content,
            'bedrooms' => get_field('bedrooms', $post->ID),
            'bathrooms' => get_field('bathrooms', $post->ID),
            'conditioner' => get_field('conditioner', $post->ID),
            'dish_washer' => get_field('dish_washer', $post->ID),
            'fai' => empty(get_field('fai',$post->ID)) ? '' : 'FAI',
            'internet' => get_field('internet', $post->ID),
            'cable_tv' => get_field('cable_tv', $post->ID),
            'balcony' => get_field('balcony', $post->ID),
            'parking_type' => get_field('parking_type', $post->ID),
            'iron' => get_field('iron', $post->ID),
            'area' => get_field('area', $post->ID),
            'attachment' => $application_file['url'],
            'property_type' => $property_type,
            'property_type_value' => $property_type_value,
            'latitude' => get_field('latitude', $post->ID),
            'longitude' => get_field('longitude', $post->ID),
            'ref_number' => get_field('refference_number', $post->ID),
            'access_handicap' => get_field('access_handicap', $post->ID),
            'smokers' => get_field('smokers', $post->ID),
            'animals' => get_field('animals', $post->ID),
            'living_area' => get_field('living_area', $post->ID),
            'label' => get_field('label', $post->ID),
            'contractor' => $contractor
        );

        /* facebook area */
        $result['share_image'] = '';
        if (!empty($thumbs[0])) {
            $result['share_image'] = $thumbs[0];
        }

        /* general column */
        $availability = get_field('availability', $post->ID);
        $seniority = get_field('seniority', $post->ID);
        $condominium = get_field('condominium', $post->ID);
        $condominium_quantity = get_field('condominium_quantity', $post->ID);
        $floor = get_field('floor', $post->ID);
        $floor_number = get_field('floor_number', $post->ID);
        $general_state = get_field('general_state', $post->ID);
        if (!empty($result['area'])) {
            $result['descriptions']['GÉNÉRAL']['Surface totale'] = $result['area'] . ' m<sup>2</sup>';
        }
        if (!empty($result['living_area'])) {
            $result['descriptions']['GÉNÉRAL']['Surface loi Carrez'] = $result['living_area'] . ' m<sup>2</sup>';
        }
        if (!empty($condominium)) {
            $result['descriptions']['GÉNÉRAL']['Copropriété'] = $condominium;
        }
        if (!empty($condominium_quantity)) {
            $result['descriptions']['GÉNÉRAL']['Nombre de lots'] = $condominium_quantity;
        }
        if (!empty($seniority)) {
            $result['descriptions']['GÉNÉRAL']['Ancienneté'] = mb_ucfirst($seniority);
        }
        if (!empty($floor)) {
            $result['descriptions']['GÉNÉRAL']['Étage'] = 'Rez-de-chaussé';
        } elseif (!empty($floor_number)) {
            $result['descriptions']['GÉNÉRAL']['Étage'] = $floor_number;
        }
        if (!empty($general_state)) {
            $result['descriptions']['GÉNÉRAL']['État général'][] = mb_ucfirst($general_state);
        }
        $result['descriptions']['GÉNÉRAL']['Disponibilité'] = $availability ? get_field('availability_date', $post->ID) : 'IMMÉDIATE';


        /* budget color */
        $charges_sum = get_field('charges_sum', $post->ID);
        $charges_info = get_field('charges_info', $post->ID);
        $sales_fees = get_field('sales_fees', $post->ID);
        $rent_type = get_field('rent_type', $post->ID);
        $leasing_fees = get_field('leasing_fees', $post->ID);

        if ($post->post_type == 'for_rent') {
            $result['descriptions']['BUDGET'] = array(
                'Prix de loyer' => $result['price'] . ' € / mois '.$result['fai']
            );
        } else {
            $result['descriptions']['BUDGET'] = array(
                'Prix de vente' => $result['price'] . ' € '.$result['fai']
            );
        }


        if (!empty($charges_sum)) {
            $result['descriptions']['BUDGET']['Charges'][] = $charges_sum . ' €';
        }
        if (!empty($charges_info)) {
            $result['descriptions']['BUDGET']['Charges'][] = $charges_info;
        }
        if (($post->post_type == 'for_sell') && !empty($sales_fees)) {
            $result['descriptions']['BUDGET']['Honoraires de vente'] = $sales_fees;
        }
        if ($post->post_type == 'for_rent') {
            if (!empty($rent_type)) {
                $result['descriptions']['BUDGET']['Loyer'] = $rent_type;
            }
            if (!empty($leasing_fees)) {
                $result['descriptions']['BUDGET']['Honoraires de mise en location'] = $leasing_fees;
            }
        }

        /* CONFORT INTERIEUR */
        $chauffage = get_field('heating', $post->ID);
        $tv_internet = get_field('tv_internet', $post->ID);
        $equipment = get_field('equipment', $post->ID);
        $furniture = get_field('furniture', $post->ID);
        $rooms = get_field('rooms', $post->ID);
        $equipped_kitchen = get_field('equipped_kitchen', $post->ID);

        if ($result['property_type_value'] != 'garage') {
            $result['descriptions']['CONFORT INTÉRIEUR'] = array(
                'Chauffage' => $chauffage == 'mixte' ? array(get_field('heating_mixed', $post->ID)) : $chauffage
            );
        }
        if (!empty($result['bedrooms'])) {
            $result['descriptions']['CONFORT INTÉRIEUR']['Chambre(s)'] = $result['bedrooms'];
        }
        if (!empty($rooms)) {
            $result['descriptions']['CONFORT INTÉRIEUR']['Pièce(s)'] = $rooms;
        }
        if (!empty($furniture) && ($result['property_type_value'] != 'garage')) {
            $result['descriptions']['CONFORT INTÉRIEUR']['Meublé'] = $furniture;
        }
        if (!empty($tv_internet)) {
            $result['descriptions']['CONFORT INTÉRIEUR']['TV - Internet'] = repeater_to_array($tv_internet);
        }
        if (!empty($equipment)) {
            $result['descriptions']['CONFORT INTÉRIEUR']['Équipement'] = repeater_to_array($equipment);
        }
        if (!empty($equipped_kitchen)) {
            $result['descriptions']['CONFORT INTÉRIEUR']['Cuisine equipée'] = $equipped_kitchen;
        }

        /* CONFORT EXTERIEUR */
        $terras = get_field('terrass', $post->ID);
        $terras_value  = get_field('terrass_value',$post->ID);

        $balcon = get_field('balcon',$post->ID);
        $balcon_value = get_field('balcon_value',$post->ID);

        $jardin = get_field('jardin',$post->ID);
        $jardin_value = get_field('jardin_value',$post->ID);

        $box = get_field('box',$post->ID);
        $box_value = get_field('box_value',$post->ID);

        $cave = get_field('cave',$post->ID);
        $cave_value = get_field('cave_value',$post->ID);

        $garage = get_field('garage',$post->ID);
        $garage_value = get_field('garage_value',$post->ID);

        $parking = get_field('parking',$post->ID);
        $parking_value = get_field('parking_value', $post->ID);

        if(!empty($terras) && !empty($terras_value)) {
            $result['descriptions']['Extérieur & annexes']['Terrasse'.' : '.$terras_value.' m<sup>2</sup>'] = '&nbsp';
        } elseif (!empty($terras)) {
            $result['descriptions']['Extérieur & annexes']['Terrasse'] = '&nbsp';
        }
        if(!empty($balcon) && !empty($balcon_value)) {
            $result['descriptions']['Extérieur & annexes']['Balcon(s)'.' : '.$balcon_value] = '&nbsp';
        } elseif (!empty($balcon)) {
            $result['descriptions']['Extérieur & annexes']['Balcon'] = '&nbsp';
        }
        if(!empty($jardin) && !empty($jardin_value)) {
            $result['descriptions']['Extérieur & annexes']['Jardin'.' : '.$jardin_value.' m<sup>2</sup>'] = '&nbsp';
        } elseif (!empty($jardin)) {
            $result['descriptions']['Extérieur & annexes']['Jardin'] = '&nbsp';
        }
        if(!empty($box) && !empty($box_value)) {
            $result['descriptions']['Extérieur & annexes']['Box'.' : '.$box_value.' m<sup>2</sup>'] = '&nbsp';
        } elseif (!empty($box)) {
            $result['descriptions']['Extérieur & annexes']['Box'] = '&nbsp';
        }
        if(!empty($cave) && !empty($cave_value)) {
            $result['descriptions']['Extérieur & annexes']['Cave '.' : '.$cave_value.' m<sup>2</sup>'] = '&nbsp';
        } elseif (!empty($cave)) {
            $result['descriptions']['Extérieur & annexes']['Cave'] = '';
        }
        if(!empty($garage) && !empty($garage_value)) {
            $result['descriptions']['Extérieur & annexes']['Garage'.' : '.$garage_value.' m<sup>2</sup>'] = '&nbsp';
        } elseif (!empty($garage)) {
            $result['descriptions']['Extérieur & annexes']['Garage'] = '&nbsp';
        }
        if (!empty($parking) && !empty($parking_value)) {
            $result['descriptions']['Extérieur & annexes']['Parking'.' : '.$parking_value] = '&nbsp';
        } elseif (!empty($parking)) {
            $result['descriptions']['Extérieur & annexes']['Parking'] = '&nbsp';
        }

        /* AUTRES */
        $accessibility = get_field('accessibility', $post->ID);
        $elevator = get_field('elevator', $post->ID);
        if (!empty($accessibility)) {
            $result['descriptions']['Accecibilité']['Proximité'] = repeater_to_array($accessibility);
        }
        if (!empty($elevator)) {
            $result['descriptions']['Accecibilité']['Ascenseur'] = $elevator;
        }
        if (!empty($result['access_handicap'])) {
            $result['descriptions']['Accecibilité']['Accès handicapé'] = 'oui';
        } else {
            $result['descriptions']['Accecibilité']['Accès handicapé'] = 'non';
        }

        /* DIAGNOSTICS */
        $diagnostics = get_field('dpe_images', $post->ID);
        if (!empty($diagnostics)) {
            $result['descriptions']['DIAGNOSTICS']['images'] = images_to_array($diagnostics,'thumbnail');
        }

    }


    return $result;
}

function repeater_to_array($repeater) {
    $result = array();

    foreach ($repeater as $item) {
        foreach ($item as $field) {
            $result[] = $field;
        }
    }

    return $result;
}

function images_to_array($images, $size) {
    $result = array();

    foreach ($images as $image) {
        $result[] = $image['sizes'][$size];
    }

    return $result;
}