<?php
function handle_contact_form() {
    $result = ["success" => false, "error" => "Sorry connection problem!"];

    if (isset($_POST['contact'])){
        $contact = $_POST['contact'];

        if (empty($contact['nom']) or empty($contact['ville'] or empty($contact['phone'])) or empty($contact['message'])) {
            $result["success"] = false;
            $result["error"] = "Remplir ce champ est obligatoire!";
        }
        elseif (!filter_var($contact['email'], FILTER_VALIDATE_EMAIL)) {
            $result["success"] = false;
            $result["error"] = "Veuillez vérifier votre saisie!";
        }
        else {
            $contact['nom'] = htmlspecialchars(trim($contact['nom']));
            $contact['ville'] = htmlspecialchars(trim($contact['ville']));
            $contact['phone'] = htmlspecialchars(trim($contact['phone']));
            $contact['message'] = htmlspecialchars(trim($contact['message']));

            /* filling in main email fields */
            $to = get_option('admin_email');
            $subject = "Formulaire de contact général";
            $message = "";
            $message .= "Votre nom : " . $contact['nom'] . "\n";
            $message .= "Ville : " . $contact['ville'] . "\n";
            $message .= "Téléphone : " . $contact['phone'] ."\n";
            $message .= "Votre message : " . $contact['message'] . '.';

            /* sending an email */
            if(wp_mail($to, $subject, $message)){
                $result["success"] = true;
                $result["error"] = "Votre message a été envoyé!";
            }
        }
    }

    echo json_encode($result);
    exit;
}