<?php
function get_search_values() {
    $result = array();
    $counts = array();
    global $wpdb;

    /* getting the min and max values to count */
//    $max_price = "SELECT max(cast(meta_value as unsigned)) FROM {$wpdb->postmeta} WHERE meta_key='price'";
    $max_price = "SELECT max(cast(pm.meta_value as unsigned)) FROM {$wpdb->posts} p LEFT JOIN {$wpdb->postmeta} pm on p.ID = pm.post_id WHERE pm.meta_key='price' AND (p.post_type = 'for_sell' OR p.post_type = 'for_rent')";
//    $min_price = "SELECT min(cast(meta_value as unsigned)) FROM {$wpdb->postmeta} WHERE meta_key='price'";
    $min_price = "SELECT min(cast(pm.meta_value as unsigned)) FROM {$wpdb->posts} p LEFT JOIN {$wpdb->postmeta} pm on p.ID = pm.post_id WHERE pm.meta_key='price' AND (p.post_type = 'for_sell' OR p.post_type = 'for_rent')";
    $max_bathrooms = "SELECT max(cast(meta_value as unsigned)) FROM {$wpdb->postmeta} WHERE meta_key='bathrooms'";
    
    $result['max_price'] = $wpdb->get_var($max_price);
    $result['min_price'] = $wpdb->get_var($min_price);
    $counts['max_bathrooms'] = $wpdb->get_var($max_bathrooms);


    $result['max_bathrooms'] = count_to_number($counts['max_bathrooms']);
    $result['max_bedrooms'] = count_to_number(4);

    /* get all property types */
    $property_type_object = get_field_object('field_58b004f87b3f0');
    $result['property_type'] = $property_type_object['choices'];

    /* post types */
    $property_type_object = get_field_object('field_58b004f87b3f0');
    $result['property_type'] = $property_type_object['choices'];

    /* seniority */
    $seniority_field_object = get_field_object('field_58b00c62ecf1f');
    $result['seniority'] = $seniority_field_object['choices'];

    /* ACCÈS HANDICAPÉ */
    $seniority_field_object = get_field_object('field_58b00c62ecf1f');
    $result['seniority'] = $seniority_field_object['choices'];

    /* ASCENSEUR */
    $seniority_field_object = get_field_object('field_58b00c62ecf1f');
    $result['seniority'] = $seniority_field_object['choices'];

    return $result;
}

function count_to_number($number,$counter = 1) {
    $result = array();

    $count = $counter;
    while ($number + 1  > $count) {
        $result[] = $count;
        $count++;
    }

    return $result;
}

function add_selected($name, $value) {
    if (isset($_GET[$name]) and $_GET[$name] == $value) {
        return 'selected';
    }
    return '';
}
function add_checked($name) {
    if (isset($_GET[$name]) and !empty($_GET[$name])) {
        return 'checked';
    }
    
    return '';
}
function get_value($name, $value = '') {
    if (!empty($_GET[$name])) {
        return $_GET[$name];
    }
    return $value;
}