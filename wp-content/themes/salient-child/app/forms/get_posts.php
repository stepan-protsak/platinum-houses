<?php
function get_platinum_posts($attributes, $coordinates = array(), $pid = false) {
    $result = array();

    if (!empty($attributes['post_type'])) {
        $post_type = $attributes['post_type'];
    }
    else {
        $post_type = array('for_rent','for_sell');
    }


    $args = array(
        'post_type' => $post_type,
        'posts_per_page' => 3
    );

    if (!empty($pid)){
        $args['post__not_in'] = array($pid);
    }

    if (!empty($coordinates)) {
        $lat = deg2rad($coordinates['latitude']);
        $lon = deg2rad($coordinates['longitude']);
        $rad = 50;
        $args['posts_per_page'] = 30;
        $max = $coordinates['price'] + 50000;
        $min = $coordinates['price'] - 50000;
        if ($min < 0) {
            $min = 0;
        }

        $args['meta_query'][] = array(
            'key' => 'price',
            'value' => array($min, $max),
            'type' => 'numeric',
            'compare' => 'BETWEEN'
        );

        if (!empty($attributes['property_type'])) {
            $args['meta_query'][] = array(
                'key' => 'property_type',
                'value' => $attributes['property_type'],
                'compare' => '=',
            );
        }

        add_filter('posts_where', function($where) use($lat, $lon, $rad) {
            global $wpdb;
            $where .= "AND ( 6371 * acos( cos( '".$lat."' ) * cos( radians( 
            (SELECT {$wpdb->postmeta}.meta_value FROM {$wpdb->postmeta} WHERE  {$wpdb->postmeta}.meta_key = 'latitude' AND {$wpdb->postmeta}.post_id = {$wpdb->posts}.ID) ) ) * cos( radians( 
            (SELECT {$wpdb->postmeta}.meta_value FROM {$wpdb->postmeta} WHERE  {$wpdb->postmeta}.meta_key = 'longitude' AND {$wpdb->postmeta}.post_id = {$wpdb->posts}.ID) ) - '".$lon."' ) + sin( '".$lat."' ) * sin( radians( 
            (SELECT {$wpdb->postmeta}.meta_value FROM {$wpdb->postmeta} WHERE  {$wpdb->postmeta}.meta_key = 'latitude' AND {$wpdb->postmeta}.post_id = {$wpdb->posts}.ID) ) ) ) ) < '".$rad."' ";

            return $where;
        });
    }

    $posts = new WP_Query($args);

    foreach ($posts->posts as $post) {

        $images = get_field('imagese',$post->ID);
        $price = get_field('price', $post->ID);

        if (!empty($images[0]['sizes']['portfolio-thumb'])) {
            $image = $images[0]['sizes']['portfolio-thumb'];
        }
        else {
            $image = '';
        }

        $result[] = array(
            'id' => $post->ID,
            'name' => strtoupper($post->post_title),
            'image' => $image,
            'price' => number_format((float)$price, 0, ',', ' '),
//            'href' => get_site_url().'/property/?id='.$post->ID,
            'href' => get_permalink($post->ID),
            'label' => get_field('label', $post->ID)
        );
    }
    
    return $result;
}