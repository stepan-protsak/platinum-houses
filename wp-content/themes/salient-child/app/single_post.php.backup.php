<?php function single_post () { ?>
    <?php ob_start(); ?>

    <?php $post = get_single_post(); ?>

    <div id="single-post-wrap">

        <div class="left-column">
            <div class="title">
                <div class="name">
                    <?php echo $post['name']; ?>
                </div>
                <div class="ref-num">
                    Réf. :<span id="reference-number"><?php echo $post['ref_number'];?></span>
                </div>
                <div class="price">
                    <?php echo $post['price'];?> €
                </div>
            </div>
            <div class="image-wrap">
                <?php if ($post['exclusive']) { ?>
                    <div class="exclusive" >
                        <div class="exclusive-badge">
                            <div>Exclusif</div>
                        </div>
                        <div class="triangle">

                        </div>
                    </div>
                <?php } ?>
                <div class="slider owl-carousel owl-theme">
                    <?php foreach ($post['images'] as $image) { ?>
                        <div class="item"><img src="<?php echo $image; ?>"/></div>
                    <? } ?>
                </div>
            </div>

        </div>

        <div class="right-column">
            <div class="first-block" id="trigger-modal">
                CONTACTEZ-NOUS
            </div>

            <div class="second-block">
                <a href="<?php echo $post['attachment'];?>" target="_blank">Un coup de <i class="fa fa-heart" aria-hidden="true"></i> pour un bien ? <br/> DOSSIER DE CANDIDATURE <img src="/wp-content/themes/salient-child/images/icons/download.png" /></a>
            </div>

            <div class="info">
                <div class="property-type">
                    <?php echo $post['property_type']; ?>
                </div>
                <?php if (!empty($post['area'])) {?>
                    <div class="area">
                        Superficie	<?php echo $post['area']; ?> m2
                    </div>
                <?php } ?>
                <div class="info-icons">
                    <?php if (!empty($post['bedrooms'])) { ?>
                        <div class="block-icon">
                            <?php if ($post['property_type_value'] == 'office') {?>
                                <img height="31" width="31" src="/wp-content/themes/salient-child/images/icons/office.png"/>
                            <?php } else {?>
                                <img src="/wp-content/themes/salient-child/images/icons/bed.png"/>
                            <?php } ?>
                            <div class="block-quantity"><?php echo $post['bedrooms']; ?></div>
                        </div>
                    <?php } ?>
                    <?php if (!empty($post['bathrooms'])) { ?>
                        <div class="block-icon">
                            <img src="/wp-content/themes/salient-child/images/icons/shower.png"/>
                            <div class="block-quantity"><?php echo $post['bathrooms']; ?> </div>
                        </div>
                    <?php } ?>
                    <?php if (!empty($post['parking_type'])) { ?>
                        <div class="block-icon">
                            <?php if ($post['parking_type'] == 'garage') { ?>
                                <img src="/wp-content/themes/salient-child/images/icons/garage.png"/>
                            <?php } elseif ($post['parking_type'] == 'parking') {?>
                                <img src="/wp-content/themes/salient-child/images/icons/parking.png"/>
                            <?php } elseif ($post['parking_type'] == 'none' && ($post['property_type_value'] != 'garage')) {?>
                                <img src="/wp-content/themes/salient-child/images/icons/garage_crossed.png"/>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <?php if (!empty($post['access_handicap'])) { ?>
                        <div class="block-icon">
                            <img src="/wp-content/themes/salient-child/images/icons/handicap.png"/>
                        </div>
                    <?php } ?>
                </div>

            </div>

            <div class="description">
                <?php echo $post['description']; ?>
            </div>

            <div class="badges">
                <?php if ($post['post_type'] == 'for_rent') { ?>
                    <div class="badge">
                        <?php if (!empty($post['animals'])) {?>
                            <img src="/wp-content/themes/salient-child/images/icons/checked.png" />
                        <?php } else { ?>
                            <img src="/wp-content/themes/salient-child/images/icons/un_checked.png" />
                        <?php } ?>
                        <span>Animaux</span>
                    </div>
                    <div class="badge">
                        <?php if (!empty($post['smokers'])) {?>
                            <img src="/wp-content/themes/salient-child/images/icons/checked.png" />
                        <?php } else { ?>
                            <img src="/wp-content/themes/salient-child/images/icons/un_checked.png" />
                        <?php } ?>
                        <span>Fumeur</span>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div id="single-description">
        <!--<h1><?php /*echo 'CARACTERISTIQUES';*/?></h1>-->
        <?php foreach ($post['descriptions'] as $name => $description) { ?>
            <div class="description-column">
                <div class="description-heading">
                    <?php echo mb_ucfirst(mb_strtolower($name,"UTF-8")); ?>
                </div>
                <?php foreach ($description as $key => $value) { ?>
                    <?php if (!empty($value)) { ?>
                        <div class="description-row">
                            <?php if (($key != 'images')) { ?>
                                <div class="description-name">
                                    <?php echo mb_ucfirst(mb_strtolower($key,"UTF-8")); ?>
                                </div>
                                <div class="description-value">
                                    <?php if (is_array($value)) { ?>
                                        <?php foreach ($value as $key1 => $item) {?>
                                            <?php if ($key1 != (count($value) - 1)) { ?>
                                                <?php echo $item.', '?>
                                            <?php } else {?>
                                                <?php echo $item; ?>
                                            <?php }?>
                                        <?php } ?>
                                    <?php } else {?>
                                        <?php echo $value; ?>
                                    <?php } ?>
                                </div>
                            <?php } else {?>
                                <?php foreach ($value as $image) { ?>
                                    <img src="<?php echo $image; ?>" alt="<?php echo $key; ?>">
                                <?php } ?>
                            <?php } ?>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
        <?php } ?>
        <br clear="all"/>
    </div>

    <?php if (!empty($post['latitude']) and !empty($post['longitude'])) { ?>
        <?php $posts = get_platinum_posts(array(),array('latitude'=>$post['latitude'], 'longitude' => $post['longitude'], 'price' => $post['price']),$post['id']); ?>
        <?php echo show_location_on_the_map($post['latitude'],$post['longitude']); ?>
    <?php } ?>
    <?php if (!empty($posts)) { ?>
        <?php echo similar_properties($posts);?>
    <?php } ?>
    <?php $output = ob_get_contents(); ?>
    <?php ob_end_clean(); ?>
    <?php return $output; ?>
<?php } ?>
