<?php
function init_google_maps($callback) { ?>
    <?php $options = get_option('salient_redux'); ?>
    <?php ob_start();?>
        
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=<?php echo $options['google-maps-api-key']; ?>&region=FR&libraries=places&callback=<?php echo $callback; ?>">
        </script>

    <?php $output = ob_get_contents(); ?>
    <?php ob_end_clean(); ?>
    <?php return $output; ?>
<?php } ?>