<?php function similar_properties($posts = array()) { ?>
    <?php ob_start();?>
        <div data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row standard_section" style="padding-top: 0px; padding-bottom: 0px; margin-bottom: 10px;"><div class="row-bg-wrap instance-2"> <div class="row-bg    " style="" data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div> </div><div class="col span_12 dark left">
                <div class="vc_col-sm-12 box-title-wrap wpb_column column_container vc_column_container col no-extra-padding instance-2" data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">

                            <div class="wpb_text_column wpb_content_element  vc_custom_1485271746964">
                                <div class="wpb_wrapper">
                                    <p style="text-transform: uppercase;padding-bottom: 10px;">Propriétés similaires</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="clear: both;"></div>
        <br>
        <div class="posts-row-slider owl-carousel owl-theme">
            <?php foreach ($posts as $post) { ?>
                <div class="post-wrap item">
                    <div class="top-box">
                        <?php if ($post['label']) { ?>
                            <div class="exclusive" >
                                <div class="exclusive-badge">
                                    <div><?php echo $post['label'];?></div>
                                </div>
                                <div class="triangle">
                                </div>
                            </div>
                        <?php } ?>
                        <a href="<?php echo $post['href']; ?>"><img src="<?php echo $post['image'];?>" /></a>
                    </div>
                    <div class="bottom-box">
                        <div class="name">
                            <a href="<?php echo $post['href']; ?>" ><?php echo $post['name'];?></a>
                        </div>
                        <div class="price">
                            <a href="<?php echo $post['href']; ?>" ><?php echo $post['price'];?>€</a>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    <?php $output = ob_get_contents(); ?>
    <?php ob_end_clean(); ?>
    <?php return $output; ?>
<?php } ?>
