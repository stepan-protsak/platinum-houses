<?php function search_box() { ?>
    <?php $options = get_search_values(); ?>
    <?php ob_start();?>
        <div class="search-wrap">
            <form method="get" action="<?php echo get_page_link(188);?>">
                <div class="top-box-wrap">
                    <div class="top-box">
                        <input type="text" placeholder="Où voulez-vous vivre" onkeyup="autoCompleteInit(jQuery(this).val())" name="location" value="<?php echo get_value('location');?>" />
                    </div>
                    <div class="bottom-box">
                        <select name="property_for" title="Type de transaction">
                            <option value="0" disabled selected style="display: none;" >Type de transaction</option>
                            <option value="for_sell" <?php echo add_selected('property_for', 'for_sell' );?> >Vente</option>
                            <option value="for_rent" <?php echo add_selected('property_for', 'for_rent' );?> >Location</option>
                        </select>
                    </div>
                </div>
                <div class="bottom-box">

                    <div class="post-type">
                        <select name="property_type" title="Type de bien">
                            <option value="0" disabled selected style="display: none;" >Type de bien</option>
                            <?php foreach ($options['property_type'] as $key => $type) { ?>
                                <option value="<?php echo $key; ?>" <?php echo add_selected('property_type', $key);?> ><?php echo mb_ucfirst(mb_strtolower($type,"UTF-8")); ?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="number-of-bedrooms">
                        <select name="bedrooms" title="Nombre  de pieces">
                            <option value="0" disabled selected style="display: none;" >Nombre  de pièces</option>
                            <?php foreach ($options['max_bedrooms'] as $value) { ?>
                                <option value="<?php echo $value; ?>" <?php echo add_selected('bedrooms', $value);?> ><?php echo $value; ?></option>
                            <?php } ?>
                            <option value="more" <?php echo add_selected('bedrooms', 'more');?> >5 +</option>
                        </select>
                    </div>

                    <div class="price-block">

                        <div class="label">
                            Votre budget max
                        </div>

                        <div class="inputs">
                            <input type="hidden" name="max_price" id="max_price1" class="max_price" data-max="<?php echo $options['max_price'];?>" value="<?php echo get_value('max_price',$options['max_price']);?>" title="Votre budget max">
                            <input type="hidden" name="min_price" id="min_price1" class="min_price" data-max="<?php echo $options['min_price'];?>" value="<?php echo get_value('min_price',$options['min_price']);?>" title="Votre budget min">
                        </div>

                        <div class="price-slider">
                            <div id="price-range" class="price-range">

                            </div>

                            <div class="price-display">
                                <span id="min_price_display" class="min_price_display"></span>
                                <span id="max_price_display" class="max_price_display"></span>
                            </div>
                        </div>
                    </div>

                    <div class="search-button">
                        <button type="submit">C'est parti</button>
                    </div>

                    <input type="hidden" name="c_page" value="1" >

                </div>
                <div class="hidden-fields">
                    <div class="hidden-selects">
                        <select name="seniority" title="ANCIENNETÉ">
                            <option value="0" disabled selected style="display: none;" >Ancienneté</option>
                            <?php foreach ($options['seniority'] as $key => $option) { ?>
                                <option value="<?php echo $key; ?>" <?php echo add_selected('seniority', $key);?> ><?php echo mb_ucfirst(mb_strtolower($option,"UTF-8"))?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="hidden-input">
                        <input type="text" name="area" value="<?php echo get_value('area');?>" placeholder="Surface minimum" >
                    </div>

                    <div class="hidden-selects switch">
                        <span class="switch-title" >Accès handicapé :</span>
                        <input type="checkbox" class="access_handicap" <?php echo add_checked('access_handicap'); ?> name="access_handicap" value="1" title="Accès handicapé" >
                    </div>

                    <div class="hidden-selects switch">
                        <span class="switch-title" >Ascenseur :</span>
                        <input type="checkbox" class="elevator" <?php echo add_checked('elevator'); ?> name="elevator" value="oui" title="Ascenseur" >
                    </div>

                </div>
                <div class="hidden-search-button search-button">
                    <button type="button"><span id="criteries-sign">+</span> DE CRITÈRES</button>
                </div>
            </form>
        </div>
    <?php $output = ob_get_contents(); ?>
    <?php ob_end_clean(); ?>
    <?php return $output; ?>
<?php } ?>
