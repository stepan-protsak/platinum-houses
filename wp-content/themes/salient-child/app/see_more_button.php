<?php function see_more_button($args) { ?>
    <?php ob_start();?>
        <div class="see-more">
            <a href="<?php echo get_page_link(188).'?property_type='.$args['type']; ?>">
                <span>Découvrir</span>
                <img src="/wp-content/themes/salient-child/images/icons/see_more.png">
            </a>
        </div>
    <?php $output = ob_get_contents(); ?>
    <?php ob_end_clean(); ?>
    <?php return $output; ?>
<?php } ?>
