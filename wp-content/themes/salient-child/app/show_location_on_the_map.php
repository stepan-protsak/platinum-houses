<?php function show_location_on_the_map($latitude,$longitude){ ?>
    <?php ob_start();?>
        <input type="hidden" id="latitude" value="<?php echo $latitude;?>" disabled >
        <input type="hidden" id="longitude" value="<?php echo $longitude;?>" disabled >
        <div class="cover" >
            <div class="wrapper" onclick="jQuery(this).remove()"></div>
            <div id="map" class="single-page" style="height: 550px; width: 100%">

            </div>
        </div>
        <?php echo addJs('show_location_on_the_map'); ?>
        <?php echo init_google_maps('initMap'); ?>
    <?php $output = ob_get_contents(); ?>
    <?php ob_end_clean(); ?>
    <?php return $output; ?>
<?php } ?>