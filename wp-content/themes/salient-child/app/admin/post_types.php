<?php

function flat_location_layout($post) {
    $values = get_post_custom($post->ID);
    $latitude = isset( $values['latitude'] ) ? esc_attr($values['latitude'][0] ) : "";
    $longitude = isset( $values['longitude'] ) ? esc_attr($values['longitude'][0]) : "";
    wp_nonce_field( 'my_meta_box_nonce', 'meta_box_nonce' );
    echo '<input type="text" name="location" id="location" style="width:100%" placeholder="Input the location name"/>';
    echo '<input type="hidden" name="latitude" id="latitude" value="'.$latitude.'"/>';
    echo '<input type="hidden" name="longitude" id="longitude" value="'.$longitude.'" />';
    echo addJs('admin_auto_complete');
    echo init_google_maps('initAutocomplete');
}

function save_location_info($post_id){
    if( !isset( $_POST['meta_box_nonce'] ) || !wp_verify_nonce( $_POST['meta_box_nonce'], 'my_meta_box_nonce' ) ) return;
    if( !current_user_can( 'edit_post' ) ) return;

    if (isset($_POST['latitude'])) {
        update_post_meta($post_id, 'latitude', $_POST['latitude']);
    }
    if (isset($_POST['longitude'])) {
        update_post_meta($post_id, 'longitude', $_POST['longitude']);
    }
}
add_action( 'save_post', 'save_location_info' );


function platinum_sell_set_meta_boxes() {
    add_meta_box('house-location', 'Carte', 'flat_location_layout', 'for_sell', 'advanced', 'default');
}
function platinum_rent_set_meta_boxes() {
    add_meta_box('house-location', 'Carte', 'flat_location_layout', 'for_rent', 'advanced', 'default');
}