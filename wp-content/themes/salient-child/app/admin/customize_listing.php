<?php
/**
 * Inserting values in a custom column.
 */
add_action('manage_for_rent_posts_custom_column','customize_posts_columns',10, 2);
add_action('manage_for_sell_posts_custom_column','customize_posts_columns',10, 2);
function customize_posts_columns($column, $post_id ) {
    switch ($column) {
        case 'reference':
            echo get_post_meta($post_id,'refference_number',true);
            break;
        default:
            return;
    }
}

/**
 * Adding custom column to posts listing.
 */
add_filter('manage_edit-for_rent_columns', 'add_posts_columns');
add_filter('manage_edit-for_sell_columns', 'add_posts_columns');
function add_posts_columns($columns) {
    $columns['reference'] = 'Ref.';
    return $columns;
}

/**
 * Making column sortable.
 */
add_filter( 'manage_edit-for_rent_sortable_columns', 'make_posts_columns_sortable' );
add_filter( 'manage_edit-for_sell_sortable_columns', 'make_posts_columns_sortable' );
function make_posts_columns_sortable($columns) {
    $columns['reference'] = 'ref';
    return $columns;
}

/**
 * Making column sortable.
 */
add_action( 'pre_get_posts', 'make_ref_sortable' );
function make_ref_sortable($query) {
    if ($query->get('orderby') == 'ref') {
        $query->set('meta_key','refference_number');
        $query->set('orderby','meta_value');
    }
}

/**
 * hook the posts search if we're on the admin page for our type
 */
add_action('admin_init', 'admin_init_for_custom_post_types');
function admin_init_for_custom_post_types() {
    global $typenow;

    if ( ($typenow === 'for_rent') || ($typenow === 'for_sell') ) {
        add_filter('posts_search', 'posts_search_for_custom_post_types', 10, 2);
    }
}

/**
 * add query condition for custom meta
 * @param string $search the search string so far
 * @param WP_Query $query
 * @return string
 */
function posts_search_for_custom_post_types($search, $query) {
    global $wpdb;

    if ($query->is_main_query() && !empty($query->query['s'])) {
        $sql = "
            or exists (
                select * from {$wpdb->postmeta} where post_id={$wpdb->posts}.ID
                and meta_key in ('refference_number')
                and meta_value like %s
            )
        ";
        $like   = '%' . $wpdb->esc_like($query->query['s']) . '%';
        $search = preg_replace("#\({$wpdb->posts}.post_title LIKE [^)]+\)\K#", $wpdb->prepare($sql, $like), $search);
    }

    return $search;
}

/**
 * Register and enqueue a custom stylesheet in the WordPress admin.
 */
function wpdocs_enqueue_custom_admin_style() {
    wp_register_style( 'custom_wp_admin_css', '/wp-content/themes/salient-child/css/admin.css', false, '1.0.0' );
    wp_enqueue_style( 'custom_wp_admin_css' );
}
add_action( 'admin_enqueue_scripts', 'wpdocs_enqueue_custom_admin_style' );