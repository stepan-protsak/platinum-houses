<?php function search_form() { ?>
    <?php $options = get_search_values(); ?>
    <?php ob_start();?>
        <!--<div class="search-form-wrap container">
            <form method="get" action="">
                <div class="top-box">

                    <div class="location">
                        <input type="text" name="location" placeholder="Où voulez-vous vivre" onkeyup="autoCompleteInit(jQuery(this).val())" value="<?php /*echo get_value('location');*/?>" >
                    </div>

                    <div class="bathrooms">
                        <select name="bathrooms" title="Salles de bain">
                            <option value="0" disabled selected style="display: none;" >Salles de bain</option>
                            <?php /*foreach ($options['max_bathrooms'] as $value) { */?>
                                <option value="<?php /*echo $value; */?>" <?php /*echo add_selected('bathrooms', $value);*/?> ><?php /*echo $value; */?></option>
                            <?php /*} */?>
                        </select>
                    </div>

                </div>

                <div class="bottom-box">

                    <div class="property_type">
                        <select name="property_type" title="Type de bien">
                            <option value="0" disabled selected style="display: none;" >Type de bien</option>
                            <?php /*foreach ($options['property_type'] as $key => $type) { */?>
                                <option value="<?php /*echo $key; */?>" <?php /*echo add_selected('property_type', $key);*/?> ><?php /*echo mb_convert_case(mb_strtolower($type,"UTF-8"),MB_CASE_TITLE,"UTF-8"); */?></option>
                            <?php /*} */?>
                        </select>
                    </div>

                    <div class="bedrooms">
                        <select name="bedrooms" title="Nombre de pieces">
                            <option value="0" disabled selected style="display: none;" >Nombre de pièces</option>
                            <?php /*foreach ($options['max_bedrooms'] as $value) { */?>
                                <option value="<?php /*echo $value; */?>" <?php /*echo add_selected('bedrooms', $value);*/?> ><?php /*echo $value; */?></option>
                            <?php /*} */?>
                            <option value="more" <?php /*echo add_selected('bedrooms', 'more');*/?> >5 +</option>
                        </select>
                    </div>

                    <div class="price-block">

                        <div class="label">
                            Votre budget max
                        </div>
                        <div class="inputs">
                            <input type="hidden" name="max_price" id="max_price" class="max_price" data-max="<?php /*echo $options['max_price'];*/?>" value="<?php /*echo get_value('max_price',$options['max_price']);*/?>" title="Votre budget max">
                            <input type="hidden" name="min_price" id="min_price" class="min_price" data-max="<?php /*echo $options['min_price'];*/?>" value="<?php /*echo get_value('min_price',$options['min_price']);*/?>" title="Votre budget min">
                        </div>

                        <div class="price-slider">
                            <div id="price-range1" class="price-range" style="width: 100%">

                            </div>

                            <div class="price-display">
                                <span id="min_price_display" class="min_price_display"></span>
                                <span id="max_price_display" class="max_price_display"></span>
                            </div>
                        </div>
                    </div>


                    <div class="submit-button">
                        <button type="submit">
                            C'est parti
                        </button>
                    </div>
                </div>
                <div class="hidden-fields">
                    <div class="hidden-selects">
                        <select name="seniority" title="ANCIENNETÉ">
                            <option value="0" disabled selected style="display: none;" >Ancienneté</option>
                            <?php /*foreach ($options['seniority'] as $key => $option) { */?>
                                <option value="<?php /*echo $key; */?>" <?php /*echo add_selected('seniority', $key);*/?> ><?php /*echo mb_convert_case(mb_strtolower($option,"UTF-8"),MB_CASE_TITLE,"UTF-8"); */?></option>
                            <?php /*} */?>
                        </select>
                    </div>

                    <div class="hidden-input">
                        <input type="text" name="area" value="<?php /*echo get_value('area');*/?>" placeholder="Surface minimum" >
                    </div>

                    <div class="hidden-selects switch">
                        <span class="switch-title" >Accès handicapé :</span>
                        <input type="checkbox" class="access_handicap" <?php /*echo add_checked('access_handicap'); */?> name="access_handicap" value="1" title="Accès handicapé" >
                    </div>

                    <div class="hidden-selects switch">
                        <span class="switch-title" >Ascenseur :</span>
                        <input type="checkbox" class="elevator" <?php /*echo add_checked('elevator'); */?> name="elevator" value="oui" title="Ascenseur" >
                    </div>

                </div>
                <input type="hidden" name="c_page" value="1" >


                <div class="hidden-search-button submit-button">
                    <button type="button">+ DE CRITÈRES</button>
                </div>
            </form>
        </div>-->

    <?php $output = ob_get_contents(); ?>
    <?php ob_end_clean(); ?>
    <?php return $output; ?>
<?php } ?>
