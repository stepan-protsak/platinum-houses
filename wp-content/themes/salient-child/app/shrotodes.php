<?php
add_shortcode('search_box','search_box');
add_shortcode('contact_form', 'contact_form');
add_shortcode('show_posts', 'show_posts');
add_shortcode('search_platinum_page','search_platinum_page');
add_shortcode('search_form','search_form');
add_shortcode('single_post', 'single_post');
add_shortcode('similar_properties','similar_properties');
add_shortcode('see_more_button', 'see_more_button');
add_shortcode('search_heading','search_heading');
