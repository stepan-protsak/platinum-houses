<?php function contact_form() { ?>
    <?php ob_start();?>
        <div class="contact-form">
            <form method="post" id="contact-form" action="" onsubmit="return false;">
                <div class="form">

                    <div class="control">
                        <input type="text" placeholder="Votre nom" name="contact[nom]">
                    </div>

                    <div class="control">
                        <input type="text" placeholder="Ville" name="contact[ville]">
                    </div>

                    <div class="control">
                        <input type="text" placeholder="Téléphone" name="contact[phone]">
                    </div>

                    <div class="control">
                        <input type="text" placeholder="Email" name="contact[email]">
                    </div>

                    <div class="control">
                        <input type="text" placeholder="Votre message" name="contact[message]">
                    </div>

                    <div class="error-block">
                        
                    </div>
                </div>

                <div class="submit-button">
                    <button type="submit" id="contact-submit">ENVOYER UN MESSAGE</button>
                </div>
            </form>
        </div>
    <?php $output = ob_get_contents(); ?>
    <?php ob_end_clean(); ?>
    <?php return $output; ?>
<?php } ?>
