<?php
if (!function_exists('mb_ucfirst')) {
    function mb_ucfirst($str, $encoding = "UTF-8", $lower_str_end = false) {
        $first_letter = mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding);
        $str_end = "";
        if ($lower_str_end) {
            $str_end = mb_strtolower(mb_substr($str, 1, mb_strlen($str, $encoding), $encoding), $encoding);
        }
        else {
            $str_end = mb_substr($str, 1, mb_strlen($str, $encoding), $encoding);
        }
        $str = $first_letter . $str_end;
        return $str;
    }
}


add_action( 'wp_enqueue_scripts', 'salient_child_enqueue_styles');
function salient_child_enqueue_styles() {
	
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css', array('font-awesome'));

    if ( is_rtl() ) 
   		wp_enqueue_style(  'salient-rtl',  get_template_directory_uri(). '/rtl.css', array(), '1', 'screen' );
}

add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {

    show_admin_bar(false);

}

/* function for adding CSS files */
function addCss($file) {
    return '<link href="'.get_stylesheet_directory_uri().'/css/'.$file.'.css" rel="stylesheet">';
}

/* function for adding Less files */
function addLess($file) {
    return '<link href="'.get_stylesheet_directory_uri().'/css/'.$file.'.less"  rel="stylesheet/less">';
}
/* function for adding js files */
function addJs($file) {
    return '<script src="'.get_stylesheet_directory_uri().'/js/'.$file.'.js"></script>';
}

/* function for adding CDN */
function addCDN($url){
    if (preg_match('/css$/',$url)){
        return '<link href="'.$url.'" rel="stylesheet" type="text/css">';
    }
    elseif (preg_match('/js$/',$url)) {
        return '<script src="'.$url.'"></script>';
    }

    return '';
}

/* function fo adding libraries */
function addVendor($path){
    if (preg_match('/css$/',$path)){
        return '<link href="'.get_stylesheet_directory_uri().'/vendor/'.$path.'" rel="stylesheet" type="text/css">';
    }
    elseif (preg_match('/js$/',$path)) {
        return '<script src="'.get_stylesheet_directory_uri().'/vendor/'.$path.'"></script>';
    }

    return '';
}


/* function for adding CDN */
function addFontCDN($url){
    return '<link href="'.$url.'" rel="stylesheet" type="text/css">';
}

/* adding custom post types */
add_action( 'init', 'create_post_type' );
function create_post_type() {

    register_post_type('for_sell',
        array(
            'labels' => array(
                'name' 			=> __('Vente'),
                'singular_name' => __('Vente'),
                'add_new' 		=> __('Ajouter un nouveau bien en vente'),
                'new_item' 		=> __('Nouveau bien en vente'),
                'edit_item'		=> __('Modifier un bien en vente'),
                'add_new_item' 	=> __('Ajouter un nouveau bien en location'),
                'search_items' 	=> __('Recherche de biens en vente'),
            ),
            'public' => true,
            'menu_position' => 5,
            'register_meta_box_cb' => 'platinum_sell_set_meta_boxes',
            'has_archive' => false,
            'rewrite' => true,
            'supports' => array('title','editor'),
            '_builtin' => false,
            'menu_icon' => 'dashicons-admin-network',
            'publicly_queryable' => true,
            'hierarchical' => true
        )
    );

    register_post_type('for_rent',
        array(
            'labels' => array(
                'name' 			=> __('Location'),
                'singular_name' => __('Location'),
                'add_new' 		=> __('Ajouter un nouveau bien en location'),
                'new_item' 		=> __('Nouveau bien en location'),
                'edit_item'		=> __('Modifier les biens en location'),
                'add_new_item' 	=> __('Ajouter un nouveau bien en location'),
                'search_items' 	=> __('Recherche de biens en location'),
            ),
            'public' => true,
            'menu_position' => 5,
            'register_meta_box_cb' => 'platinum_rent_set_meta_boxes',
            'has_archive' => false,
            'rewrite' => true,
            'supports' => array('title','editor'),
            '_builtin' => false,
            'menu_icon' => 'dashicons-admin-network',
            'publicly_queryable' => true,
            'hierarchical' => true
        )
    );
}
function vipx_remove_cpt_slug( $post_link, $post, $leavename ) {

    if ( ! in_array( $post->post_type, array( 'for_sell','for_rent' ) ) || 'publish' != $post->post_status )
        return $post_link;

    $post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );

    return $post_link;
}
add_filter( 'post_type_link', 'vipx_remove_cpt_slug', 10, 3 );

function vipx_parse_request_tricksy( $query ) {

    // Only noop the main query
    if ( ! $query->is_main_query() )
        return;

    // Only noop our very specific rewrite rule match
    if ( 2 != count( $query->query )
        || ! isset( $query->query['page'] ) )
        return;

    // 'name' will be set if post permalinks are just post_name, otherwise the page rule will match
    if ( ! empty( $query->query['name'] ) )
        $query->set( 'post_type', array( 'post', 'for_sell','for_rent', 'page' ) );
}
add_action( 'pre_get_posts', 'vipx_parse_request_tricksy' );
/* adding admin posts CRUD */
require_once 'app/admin/post_types.php';
require_once 'app/admin/customize_listing.php';

/* handling assets */
require_once 'assets_handle.php';

/* short codes handling file */
require_once 'app/shrotodes.php';

/* pages and forms */
require_once 'app/init_google_maps.php';
require_once 'app/search_box.php';
require_once 'app/contact_form.php';
require_once 'app/show_posts.php';
require_once 'app/search_page.php';
require_once 'app/search_form.php';
require_once 'app/single_post.php';
require_once 'app/see_more_button.php';
require_once 'app/show_location_on_the_map.php';
require_once 'app/similar_properties.php';
require_once 'app/forms/handle_contact_form.php';
require_once 'app/forms/get_posts.php';
require_once 'app/forms/get_single_post.php';
require_once 'app/forms/search_platinum_posts.php';
require_once 'app/forms/get_search_values.php';
require_once 'app/forms/location_auto_complete.php';


