/* getting latitude and longitude values */
var latitude = parseFloat(document.getElementById('latitude').value);
var longitude = parseFloat(document.getElementById('longitude').value);

// console.log(latitude);
// console.log(longitude);

/* function to plot marker on the map */
function initMap() {
    var coordinates = {
        lat: latitude,
        lng: longitude
    };

    var map = new google.maps.Map(
        document.getElementById('map'), {
            zoom: 15,
            center: coordinates
        }
    );

    var marker = new google.maps.Marker({
        position: coordinates,
        map: map
    });
}