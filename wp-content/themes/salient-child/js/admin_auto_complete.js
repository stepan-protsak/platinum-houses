/* getting inputs by id */
var latitudeInput = document.getElementById('latitude');
var longitudeInput = document.getElementById('longitude');
var componentForm = {
    street_number: 'short_name',
    route: 'long_name',
    locality: 'long_name',
    administrative_area_level_1: 'short_name'
};

/* defining google api variables */
var autocomplete;
var options = {
    types: ['geocode'],
    componentRestrictions: {country: 'fr'}
};

/* disabling all inputs */
for (var component in componentForm) {
    document.getElementById('acf-field-'+component).readOnly = true;
}

/* initialising auto complete */
function initAutocomplete() {
    // Create the autocomplete object, restricting the search to geographical
    autocomplete = new google.maps.places.Autocomplete(
        (document.getElementById('location')),
        options);

    autocomplete.addListener('place_changed', fillInAddress);
}

/* filling in the address fields */
function fillInAddress() {
    var place = autocomplete.getPlace();

    /* cleaning the form if value was in it */
    for (var component in componentForm) {
        document.getElementById('acf-field-'+component).value = '';
    }

    /* getting all the locations and filling in the inputs */
    for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (componentForm[addressType]) {
            document.getElementById('acf-field-'+addressType).value = place.address_components[i][componentForm[addressType]];
        }
    }

    /* setting latitude and longitude inputs */
    latitudeInput.value = place.geometry.location.lat();
    longitudeInput.value = place.geometry.location.lng();
}

jQuery('input#title').attr('maxlength', 100);
/* hiding and showing the attr type */
/*
jQuery(document).on('change','#acf-field-property_for-sell,#acf-field-property_for-rent', function () {
    var rentOptionsBox = jQuery('#acf_318');
    var propertyForValue = jQuery(this).val();
    if (propertyForValue == 'sell') {
        rentOptionsBox.hide();
    }
    else {
        rentOptionsBox.show();
    }
});
jQuery(document).ready(function () {
    var rentOptionsBox = jQuery('#acf_318');
    var propertyForValue = jQuery('#acf-field-property_for-sell,#acf-field-property_for-rent').val();

    if (propertyForValue == 'sell') {
        rentOptionsBox.hide();
    }
    else {
        rentOptionsBox.show();
    }
});*/
