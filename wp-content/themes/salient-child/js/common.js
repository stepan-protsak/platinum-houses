jQuery(document).ready(function($){

    /* hiding one of the searches */
    var searchBox = $('#search-box-1');

    var searchCoverBlock = $('#advanced-search-1');
    var searchBoxMobile =$('#search-box-mobile');

    /* header selector */
    var header = $('#header-outer');
    var formWrap = $('#search-box-1 .col.span_12.dark.left');
    var form = $('#search-box-1 .search-wrap form');
    
    /*if($(document).scrollTop() > 0) {
        header.addClass('header-scroll');
    }*/

    /* search menu mobile version */
    searchBoxMobile.on('click',function () {
        searchBox.toggleClass('active');
        searchBox.toggleClass('active-mobile');
        $(this).toggleClass('active');
    });

    /* script for adding custom class at scroll */
    if ($(window).width() > 999) {
        if ($(document).scrollTop() > (header.height() + (formWrap.height() - form.height()))){
            searchBox.addClass('active');
            searchCoverBlock.show();
        }
        else {
            searchBox.removeClass('active');
            searchCoverBlock.hide();
        }
    } else {
        if ($(document).scrollTop() > (header.height() + (formWrap.height()))){
            $('#search-box-mobile').show();
        }
        else {
            $('#search-box-mobile').hide();
            searchBox.removeClass('active');
            searchBox.removeClass('active-mobile');
            searchBoxMobile.removeClass('active');
        }
    }

    $(document).on( 'scroll', function(){
        if ($(this).scrollTop()  == 0){
            searchBox.removeClass('active');
        }
        if ($(window).width() > 999) {
            if ($(this).scrollTop() > (formWrap.height() - form.height())) {
                searchBox.addClass('active');
                searchCoverBlock.show();
            }
            else {
                searchBox.removeClass('active');
                searchCoverBlock.hide();
            }
        } else {
            if ($(document).scrollTop() > (header.height() + (formWrap.height()))){
                $('#search-box-mobile').show();
            }
            else {
                $('#search-box-mobile').hide();
                searchBox.removeClass('active');
                searchBox.removeClass('active-mobile');
                searchBoxMobile.removeClass('active');
            }
        }
    });

    /* slider range variables*/
    var maxPriceInput = $('.max_price');
    var minPriceInput = $('.min_price');
    var minPriceDisplay = $('.min_price_display');
    var maxPriceDisplay = $('.max_price_display');
    var priceInput = $("#price-range");
    var priceInput1 = $("#price-range1");
    var maxPriceValue = parseInt(maxPriceInput.val());
    var minPriceValue = parseInt(minPriceInput.val());
    var minPriceI = parseInt(minPriceInput.data('max'));
    var maxPriceI = parseInt(maxPriceInput.data('max'));

    if (minPriceI < 0) {
        minPriceI = 0;
    }

    /* slider range init */
    priceInput.slider({
        range: true,
        min: minPriceI,
        step: 1,
        max: maxPriceI,
        values: [ minPriceValue, maxPriceValue ],
        slide: function( event, ui ) {
            minPriceDisplay.html(number_format(priceInput.slider( "values", 0 ), 0, ',', ' ')+" €");
            maxPriceDisplay.html(number_format(priceInput.slider( "values", 1 ), 0, ',', ' ')+" €");
            minPriceInput.val(priceInput.slider( "values", 0 ));
            maxPriceInput.val(priceInput.slider( "values", 1 ));
        },
        stop: function (event, ui ) {
            minPriceDisplay.html(number_format(priceInput.slider( "values", 0 ), 0, ',', ' ')+" €");
            maxPriceDisplay.html(number_format(priceInput.slider( "values", 1 ), 0, ',', ' ')+" €");
            minPriceInput.val(priceInput.slider( "values", 0 ));
            maxPriceInput.val(priceInput.slider( "values", 1 ));
        }
    });
    minPriceDisplay.html(number_format(priceInput.slider( "values", 0 ), 0, ',', ' ')+" €");
    maxPriceDisplay.html(number_format(priceInput.slider( "values", 1 ), 0, ',', ' ')+" €");
    minPriceInput.val(priceInput.slider( "values", 0 ));
    maxPriceInput.val(priceInput.slider( "values", 1 ));

    /* owl carousel singe post initialization */
    $('.slider').owlCarousel({
        items:1,
        nav: true,
        autoHeight:true,
        touchDrag: false,
        mouseDrag: false
    });

    /* owl carousel for similar posts*/
    $('.posts-row-slider.owl-carousel.owl-theme').owlCarousel({
        items:3,
        nav: true,
        dots:false,
        touchDrag: false,
        mouseDrag: false,
        margin:20,
        stagePadding: 5,
        responsive:{
            0 : {
                items: 1
            },
            450 : {
                items: 2
            },
            768 : {
                items: 3
            }
        },
        navText: ['<img src="/wp-content/themes/salient-child/images/icons/left-arrow-pink.png"/>',
                  '<img src="/wp-content/themes/salient-child/images/icons/right-arrow-pink.png"/>']

    });


    /* contact form */
    var errorBlock = $('.error-block');

    $("#contact-submit").click(function () {
        $.ajax({
            url: '/wp-admin/admin-ajax.php',
            type: 'POST',
            dataType : "json",
            data: 'action=handle_contact_form&'+$('#contact-form').serialize(),
            success: function(data){
                if (data.success) {
                    errorBlock.html(data.error);
                }
                else {
                    errorBlock.html('<span style="color: black">*'+data.error+'</span>');
                }

            }
        })
    });


    /* modal window click event */
    $('#trigger-modal').click(function(){
        $('#header-outer, #top').hide();
        $('.footer-contact-modal, .footer-contact-modal .modal-in').slideDown();
        $('html, body').css('overflow','hidden');
    });

    $('.footer-contact-modal .close').click(function(){
        $('#header-outer, #top').show();
        $('.footer-contact-modal, .footer-contact-modal .modal-in').slideUp();
        $('html, body').css('overflow','auto');
    });

    $("input[name=location]").autocomplete({
        source: [ ]
    });


    /* reference number */
    $('input[name=refference]').val($('#reference-number').text()).prop('readonly',true);


    /* hidden search button */
    $.urlParam = function(name){
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results==null){
            return null;
        }
        else{
            return results[1] || 0;
        }
    };


    var hiddenSearchButton = $('.hidden-search-button');
    var hiddenSearchWrap = $('.hidden-fields');
    /*if ($(window).width() > 768) {
        hiddenSearchWrap.height(60);
    }*/

    if ($.urlParam('area') != null) {
        hiddenSearchWrap.show();
        hiddenSearchWrap.addClass('active');
        hiddenSearchWrap.find('input[type=text], select').prop('disabled',false);
        hiddenSearchButton.find('#criteries-sign').html('-');
    }
    else {
        hiddenSearchWrap.hide();
        hiddenSearchWrap.removeClass('active');
        hiddenSearchWrap.find('input[type=text], select').prop('disabled',true);
        hiddenSearchButton.find('#criteries-sign').html('+');
    }

    hiddenSearchButton.click(function () {
        if (!hiddenSearchWrap.hasClass('active')) {
            hiddenSearchWrap.slideDown(300, 'linear');
            hiddenSearchWrap.addClass('active');
            hiddenSearchWrap.find('input[type=text], select').prop('disabled',false);
            hiddenSearchButton.find('#criteries-sign').html('-');
        }
        else {
            hiddenSearchWrap.slideUp(300, 'linear');
            hiddenSearchWrap.removeClass('active');
            hiddenSearchWrap.find('input[type=text], select').prop('disabled',true);
            hiddenSearchButton.find('#criteries-sign').html('+');
        }
    });
    // $('#search-box-1').addClass('active');
    /* adding switch buton */
    $('.access_handicap').lc_switch();
    $('.elevator').lc_switch();

});
jQuery(window).load(function () {
    /* calculation the width of the block */
    var teamBlock = jQuery('#team');
    var mapBlock = jQuery('#house').siblings('#map');
    makeBlockFullPage(mapBlock);
    makeBlockFullPage(teamBlock);
    calculateCarouselHeight();
});
function makeBlockFullPage(block) {
    block.css('width', jQuery(window).width());
    block.css('margin-left',  (jQuery('.container').width() - jQuery(window).width())/2);
}
function calculateCarouselHeight() {
    var carouselWrap = '#single-post-wrap .image-wrap .owl-stage-outer.owl-height';
    jQuery(carouselWrap).css('height', jQuery(carouselWrap + ' .owl-item.active').height());
}
function autoCompleteInit(locationValue) {

    jQuery.ajax({
        url: '/wp-admin/admin-ajax.php',
        type: 'POST',
        dataType: "json",
        data: 'action=location_auto_complete&auto_complete='+locationValue,
        success: function (data) {
            if (data.success) {
                jQuery("input[name=location]").autocomplete("option","source", data.locations);
            }
        }
    });
}

function number_format (number, decimals, dec_point, thousands_sep) {
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}
