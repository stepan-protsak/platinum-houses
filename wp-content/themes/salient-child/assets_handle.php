<?php
$minify = true;
/* CSS INCLUDING */
function hook_css(){
    global $minify;
    /* LESS */
    if($minify) {
        echo addCss('custom');
    } else {
        echo addLess('custom');
    }
    /* END */

    echo addVendor('owl-carousel/owl.carousel.min.css');
    echo addVendor('owl-carousel/owl.theme.default.min.css');

    echo addVendor('jquery-ui/jquery-ui.min.css');
    echo addVendor('switch/lc_switch.css');

//    echo addCss('modal-css');

}
add_action('wp_head', 'hook_css');

/* JS INCLUDING */
function hook_js(){
    global $minify;


    if (!$minify) {
        echo addJs('common');
        echo addVendor('switch/lc_switch.js');
        echo "<script>less = {env: 'development'};</script>";
        echo addCDN('//cdnjs.cloudflare.com/ajax/libs/less.js/2.7.1/less.min.js');
    } else {
        echo addJs('common.min');
        echo addVendor('switch/lc_switch.min.js');
    }

    echo addVendor('jquery-ui/jquery-ui.min.js');
    echo addVendor('owl-carousel/owl.carousel.min.js');


}
add_action('wp_head', 'hook_js');