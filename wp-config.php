<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'platinum');

/** MySQL database username */
define('DB_USER', 'user');

/** MySQL database password */
define('DB_PASSWORD', 'sql');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'hx1jo/[U#)/2.a~#*hfM$8Q%:?f;IEshXBqgxDY=jg8Cy`S-k}>>o?vJ<9?Qp-nh');
define('SECURE_AUTH_KEY',  '>D:J?z]RoATE 0=&yB*fQ,*jXuaV.k{qP,}eCS{<BEi/maCkRZuX(qBEuX_IkoEL');
define('LOGGED_IN_KEY',    'MkikDZ7QvQK6lK6klKklv>pmGqPa]$kl6VLdWV3)<98XrH|#&)>$p}F1KjlWystE');
define('NONCE_KEY',        ';O+87~?%M@^:;rGo{2Am2Qz(=SjvN.Y+eW^p@]X}/91a7GMN U+xEnm2/>B(f#ma');
define('AUTH_SALT',        'c%/@;4 U/Qep,pm5u]AF#kd+ps2If8O?1TilN&-FI,xLyOs*Jvn;YvMEtwT5X8My');
define('SECURE_AUTH_SALT', 'z>;>P5Uz1Q)8@Xgcek6 =}}:BKQB/] g}FW}TS0$urQpah2gh[d`TzTwb{n[5iuG');
define('LOGGED_IN_SALT',   '+pC3Y&8FoNpbsQ%mv5<t$y8o&O g[hq|8+X7@nu#nzl<2FGm^``Up5#T,{Ycf{ig');
define('NONCE_SALT',       '!d/~Q(+&.STky?SLL8$e!+l+{NH&WGXx*xU7-ueT*+Q#R?!{=|aQl< |7dS/2]gd');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'pl_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
